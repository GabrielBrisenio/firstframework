Pod::Spec.new do |spec|

  spec.name         = "FirstFramework"
  spec.version      = "1.0.1"
  spec.summary      = "A short description of FirstFramework."
  spec.description  = "Long description, este es un demod d eintegración de framework con jfrog"
  spec.homepage     = "https://survey.admin.zeusgs.com.mx/"
  spec.license      = "MIT"
  spec.author       = { "Gabriel Briseño" => "gabriel_bris1315@outlook.com" }
  spec.platform     = :ios, "10"
  spec.swift_version = "5.0"
  spec.source       = { :git => "https://GabrielBrisenio@bitbucket.org/GabrielBrisenio/firstframework.git", :tag => "#{spec.version}" }
  spec.source_files  = "FirstFramework/**/*.{h,m,swift}"
end
