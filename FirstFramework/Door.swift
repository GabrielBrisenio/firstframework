//
//  Door.swift
//  FirstFramework
//
//  Created by Gabriel Briseño on 04/11/21.
//

import UIKit

public class Door {
    public static func openModule() -> UIViewController {
        let vc = UIViewController()
        vc.view.backgroundColor = .purple
        
        return vc
    }
}
